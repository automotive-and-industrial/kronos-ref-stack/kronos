/*
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/dts-v1/;

#include <mem.h>
#include <arm64/armv8-r.dtsi>
#include <dt-bindings/interrupt-controller/arm-gic.h>
#include "fvp_rd_kronos_safety_island_device_mem.dtsi"

#define IRQ_CRITICAL_PRIORITY 0x50

/ {
	#address-cells = <2>;
	#size-cells = <1>;
	model = "FVP RD-Kronos Safety Island Cluster 1";

	chosen {
		zephyr,sram = &sram0;
		zephyr,console = &uart0;
		zephyr,shell-uart = &uart0;
	};

	cpus {
		#address-cells = <1>;
		#size-cells = <0>;

		cpu@0 {
			device_type = "cpu";
			compatible = "arm,safety-island";
			reg = <0x10000>;
		};

		cpu@1 {
			device_type = "cpu";
			compatible = "arm,safety-island";
			reg = <0x10100>;
		};

	};

	timer {
		compatible = "arm,armv8-timer";
		interrupt-parent = <&gic>;
		interrupts = <GIC_PPI 13 IRQ_TYPE_LEVEL
			      IRQ_DEFAULT_PRIORITY>,
			     <GIC_PPI 4 IRQ_TYPE_LEVEL
			      IRQ_DEFAULT_PRIORITY>,
			     <GIC_PPI 11 IRQ_TYPE_LEVEL
			      IRQ_DEFAULT_PRIORITY>,
			     <GIC_PPI 3 IRQ_TYPE_LEVEL
			      IRQ_DEFAULT_PRIORITY>;
	};

	uartclk: apb-pclk {
		compatible = "fixed-clock";
		clock-frequency = <24000000>;
		#clock-cells = <0>;
	};

	soc {
		#address-cells = <2>;
		#size-cells = <1>;
		interrupt-parent = <&gic>;

		gic: interrupt-controller@30800000 {
			compatible = "arm,gic-v3", "arm,gic";
			reg = <0x0 0x30800000 0x10000>,
			      <0x0 0x30860000 0x20000>,
			      <0x0 0x30880000 0x20000>;
			interrupt-controller;
			#interrupt-cells = <4>;
			status = "okay";
		};

		uart0: uart@2a410000 {
			compatible = "arm,pl011";
			reg = <0x0 0x2a410000 0x10000>;
			status = "disabled";
			interrupts = <GIC_SPI 7 IRQ_TYPE_LEVEL IRQ_DEFAULT_PRIORITY>;
			interrupt-names = "irq_0";
			clocks = <&uartclk>;
		};

		sram0: memory@140000000 {
			compatible = "mmio-sram";
			reg = <0x1 0x40000000 DT_SIZE_M(4)>;
		};

		pc_cl1_mbox_tx: mhuv3@2ab20000 {
			compatible = "arm,mhuv3-tx";
			#mbox-cells = <1>;
			reg = <0x0 0x2ab20000 0x1000>;
			status = "disabled";
		};

		pc_cl1_mbox_rx: mhuv3@2ab30000 {
			compatible = "arm,mhuv3-rx";
			#mbox-cells = <1>;
			reg = <0x0 0x2ab30000 0x1000>;
			interrupts = <GIC_SPI 44 IRQ_TYPE_LEVEL IRQ_DEFAULT_PRIORITY>;
			interrupt-names = "rx";
			status = "disabled";
		};

		rss_cl1_mbox_tx: mhuv3@2ac00000 {
			compatible = "arm,mhuv3-tx";
			#mbox-cells = <1>;
			reg = <0x0 0x2ac00000 0x1000>;
			status = "disabled";
		};

		rss_cl1_mbox_rx: mhuv3@2ac10000 {
			compatible = "arm,mhuv3-rx";
			#mbox-cells = <1>;
			reg = <0x0 0x2ac10000 0x1000>;
			interrupts = <GIC_SPI 93 IRQ_TYPE_LEVEL IRQ_DEFAULT_PRIORITY>;
			interrupt-names = "rx";
			status = "disabled";
		};

		cl1_cl0_mbox_tx: mhuv3@2af40000 {
			compatible = "arm,mhuv3-tx";
			#mbox-cells = <1>;
			reg = <0x0 0x2af40000 0x1000>;
			status = "disabled";
		};

		cl0_cl1_mbox_rx: mhuv3@2af50000 {
			compatible = "arm,mhuv3-rx";
			#mbox-cells = <1>;
			reg = <0x0 0x2af50000 0x1000>;
			interrupts = <GIC_SPI 81 IRQ_TYPE_LEVEL IRQ_DEFAULT_PRIORITY>;
			interrupt-names = "rx";
			status = "disabled";
		};

		cl1_cl2_mbox_tx: mhuv3@2af60000 {
			compatible = "arm,mhuv3-tx";
			#mbox-cells = <1>;
			reg = <0x0 0x2af60000 0x1000>;
			status = "disabled";
		};

		cl2_cl1_mbox_rx: mhuv3@2af70000 {
			compatible = "arm,mhuv3-rx";
			#mbox-cells = <1>;
			reg = <0x0 0x2af70000 0x1000>;
			interrupts = <GIC_SPI 91 IRQ_TYPE_LEVEL IRQ_DEFAULT_PRIORITY>;
			interrupt-names = "rx";
			status = "disabled";
		};

		system_fmu: fmu@2a510000 {
			compatible = "arm,fmu";
			reg = <0x0 0x2a510000 0x1000>;
			interrupts = <GIC_SPI 96 IRQ_TYPE_LEVEL IRQ_CRITICAL_PRIORITY>,
					<GIC_SPI 102 IRQ_TYPE_LEVEL IRQ_DEFAULT_PRIORITY>;
			interrupt-names = "critical", "non_critical";
			upstream = <&pc_gic_fmu>;
			safety = <&ssu>;
			status = "disabled";
		};

		pc_gic_fmu: fmu@2a570000 {
			compatible = "arm,fmu";
			reg = <0x0 0x2a570000 0x1000>;
			status = "disabled";
		};

		ssu: ssu@2a500000 {
			compatible = "arm,ssu";
			reg = <0x0 0x2a500000 0x1000>;
			status = "disabled";
		};
	};
};

&uart0 {
	status = "okay";
	current-speed = <115200>;
};
