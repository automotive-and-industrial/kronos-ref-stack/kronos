/*
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
 * affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/device.h>
#include <zephyr/shell/shell.h>
#include <zephyr/shell/shell_string_conv.h>

#include "zephyr/subsys/fault_mgmt/fault_mgmt.h"

#ifdef CONFIG_FAULT_MGMT_SAFETY
#include "zephyr/subsys/fault_mgmt/fault_mgmt_safety.h"
#endif

#ifdef CONFIG_FAULT_MGMT_STORAGE
#include "zephyr/subsys/fault_mgmt/fault_mgmt_storage.h"
#endif

static int print_fault_device(const struct device *dev, size_t depth, size_t index, void *cookie)
{
	size_t i;
	const struct shell *sh = (const struct shell *)cookie;
	const struct device *safety_dev;

	if (depth == 0) {
		shell_fprintf(sh, SHELL_NORMAL, "Root %zd: ", index);
		shell_print(sh, "%s", dev->name);

		safety_dev = fault_mgmt_safety_device(dev);
		if (safety_dev) {
			shell_print(sh, "\tSafety: %s", safety_dev->name);
		}
	} else {
		/* Indent line based on current tree depth */
		for (i = 0; i < depth; i++) {
			shell_fprintf(sh, SHELL_NORMAL, "\t");
		}
		shell_fprintf(sh, SHELL_NORMAL, "Slot %zd: ", index);
		shell_print(sh, "%s", dev->name);
	}

	return 0;
}

static int cmd_fault_tree(const struct shell *sh, size_t argc, char **argv, void *data)
{
	fault_mgmt_device_foreach(print_fault_device, (void *)sh);

	return 0;
}

static int parse_fault_args(const struct shell *sh, size_t argc, char **argv,
			    const struct device **dev, uint32_t *prot_id)
{
	int ret = 0;

	*dev = device_get_binding(argv[1]);
	if (*dev == NULL) {
		shell_error(sh, "Invalid device name: %s", argv[1]);
		return -EINVAL;
	}

	*prot_id = (uint32_t)shell_strtoul(argv[2], 0, &ret);
	if (ret < 0) {
		shell_error(sh, "Invalid protection ID: %s", argv[2]);
		return -EINVAL;
	}

	return 0;
}

static int handle_error(const struct shell *sh, int ec)
{
	switch (ec) {
	case 0:
		break;
	case -EINVAL:
		shell_error(sh, "Invalid argument");
		break;
	case -ENOTSUP:
		shell_error(sh, "Operation not supported");
		break;
	default:
		shell_error(sh, "Unknown error");
		break;
	}
	return ec;
}

static int cmd_fault_inject(const struct shell *sh, size_t argc, char **argv, void *data)
{
	int ret;
	const struct device *dev;
	uint32_t prot_id;

	ret = parse_fault_args(sh, argc, argv, &dev, &prot_id);
	if (ret < 0) {
		return ret;
	}

	shell_info(sh, "Injecting fault 0x%x to device %s", prot_id, dev->name);

	ret = fault_mgmt_inject(dev, prot_id);
	return handle_error(sh, ret);
}

static int cmd_fault_set_enabled(const struct shell *sh, size_t argc, char **argv, void *data)
{
	int ret;
	const struct device *dev;
	uint32_t prot_id;
	bool enabled;
	const char *action;

	ret = parse_fault_args(sh, argc, argv, &dev, &prot_id);
	if (ret < 0) {
		return ret;
	}

	enabled = shell_strtobool(argv[3], 0, &ret);
	if (ret < 0) {
		shell_error(sh, "Invalid enabled status: %s", argv[3]);
		return -EINVAL;
	}

	action = enabled ? "Enabling" : "Disabling";
	shell_info(sh, "%s fault 0x%x on device %s", action, prot_id, dev->name);

	ret = fault_mgmt_set_enabled(dev, prot_id, enabled);
	return handle_error(sh, ret);
}

static int cmd_fault_set_critical(const struct shell *sh, size_t argc, char **argv, void *data)
{
	int ret;
	const struct device *dev;
	uint32_t prot_id;
	bool critical;
	const char *status;

	ret = parse_fault_args(sh, argc, argv, &dev, &prot_id);
	if (ret < 0) {
		return ret;
	}

	critical = shell_strtobool(argv[3], 0, &ret);
	if (ret < 0) {
		shell_error(sh, "Invalid criticality status: %s", argv[3]);
		return -EINVAL;
	}

	status = critical ? "critical" : "non-critical";
	shell_info(sh, "Setting fault 0x%x on device %s as %s", prot_id, dev->name, status);

	ret = fault_mgmt_set_critical(dev, prot_id, critical);
	return handle_error(sh, ret);
}

#ifdef CONFIG_FAULT_MGMT_SAFETY
static int cmd_fault_safety_status(const struct shell *sh, size_t argc, char **argv, void *data)
{
	const struct device *dev;
	enum fault_mgmt_safety_state state;

	dev = device_get_binding(argv[1]);
	if (dev == NULL) {
		shell_error(sh, "Invalid device name: %s", argv[1]);
		return -EINVAL;
	}

	state = fault_mgmt_safety_status(dev);
	shell_info(sh, "Status: %s (0x%x)", fault_mgmt_safety_state_to_str(state), state);

	return 0;
}

static int cmd_fault_safety_control(const struct shell *sh, size_t argc, char **argv, void *data)
{
	int ret;
	const struct device *dev;
	enum fault_mgmt_safety_signal signal;
	enum fault_mgmt_safety_state state;

	dev = device_get_binding(argv[1]);
	if (dev == NULL) {
		shell_error(sh, "Invalid device name: %s", argv[1]);
		return -EINVAL;
	}

	ret = fault_mgmt_safety_signal_from_str(argv[2], &signal);
	ret = handle_error(sh, ret);
	if (ret < 0) {
		return ret;
	}

	shell_info(sh, "Signal: %s (0x%x)", argv[2], signal);
	fault_mgmt_safety_control(dev, signal);

	state = fault_mgmt_safety_status(dev);
	shell_info(sh, "State: %s (0x%x)", fault_mgmt_safety_state_to_str(state), state);

	return 0;
}
#endif

static void cmd_fault_device_name(size_t idx, struct shell_static_entry *entry)
{
	const struct device *dev = shell_device_lookup(idx, NULL);

	entry->syntax = (dev != NULL) ? dev->name : NULL;
	entry->handler = NULL;
	entry->help = NULL;
	entry->subcmd = NULL;
}

#ifdef CONFIG_FAULT_MGMT_STORAGE
void fault_history_read_callback(const struct fault_mgmt_storage_info *fault_info, void *cookie)
{
	const struct shell *sh = (const struct shell *)cookie;
	uint32_t protection_id = FAULT_MGMT_FAULT_PROTECTION_ID(&(fault_info->fault));
	const char *criticality =
		FAULT_MGMT_FAULT_IS_CRITICAL(&(fault_info->fault)) ? "critical" : "non-critical";
	const struct device *dev = device_from_handle(fault_info->fault.handle);

	shell_info(sh, "Fault received (%s): 0x%x on %s : count %llu\n", criticality, protection_id,
		   dev->name, fault_info->count);
}

static int cmd_fault_listed(const struct shell *sh, size_t argc, char **argv, void *data)
{
	uint64_t threshold_value;
	int ret = 0;
	fault_storage_stats_t record_stats = {0};

	fault_mgmt_storage_stats(&record_stats);

	if (argc > 1) {
		threshold_value = (uint64_t)shell_strtoul(argv[1], 0, &ret);
	} else {
		threshold_value = 0;
	}
	if (record_stats.total_fault == 0) {
		shell_info(sh, "No fault reported");
	} else {
		shell_info(sh, "Fault history:");
		fault_mgmt_storage_foreach(fault_history_read_callback, threshold_value,
					   (void *)sh);
	}

	return ret;
}

static void cmd_fault_summary(const struct shell *sh)
{
	fault_storage_stats_t record_stats = {0};

	fault_mgmt_storage_stats(&record_stats);
	if (record_stats.total_fault == 0) {
		shell_info(sh, "No fault reported");
		return;
	}

	shell_info(sh,
		   "____________________________________________________________________________");
	shell_info(sh,
		   "|                                                                          |");
	shell_info(sh,
		   "|                                Fault Summary                             |");
	shell_info(sh,
		   "|__________________________________________________________________________|");
	shell_info(sh,
		   "                                                                            ");
	shell_info(sh, "Number of fault reported: %llu", record_stats.total_fault);

	shell_info(sh,
		   "____________________________________________________________________________");
	shell_info(sh,
		   "                                                                            ");
	shell_info(sh, "Most reported faults:");
	if (record_stats.highest_count > 1) {
		fault_mgmt_storage_foreach(fault_history_read_callback, record_stats.highest_count,
					   (void *)sh);
	} else {
		shell_info(sh, "No fault reported more than once");
	}
	shell_info(sh,
		   "____________________________________________________________________________");
	shell_info(sh,
		   "                                                                            ");
	shell_info(sh, "Fault history:");
	fault_mgmt_storage_foreach(fault_history_read_callback, 0, (void *)sh);
	shell_info(sh,
		   "____________________________________________________________________________");
}

static void cmd_total_reported_fault(const struct shell *sh)
{
	fault_storage_stats_t record_stats = {0};

	fault_mgmt_storage_stats(&record_stats);
	shell_info(sh, "Number of fault reported: %llu", record_stats.total_fault);
}

static void cmd_clear_stored_fault(const struct shell *sh)
{
	shell_info(sh, "Erasing the storage...");
	fault_mgmt_storage_clear();
	shell_info(sh, "Done!");
}
#endif

SHELL_DYNAMIC_CMD_CREATE(dsub_device_name, cmd_fault_device_name);

SHELL_STATIC_SUBCMD_SET_CREATE(
	fault, SHELL_CMD_ARG(tree, NULL, "Enumerate the fault tree", cmd_fault_tree, 1, 0),
	SHELL_CMD_ARG(inject, &dsub_device_name, "Inject a fault", cmd_fault_inject, 3, 0),
	SHELL_CMD_ARG(set_enabled, &dsub_device_name, "Enable/disable a fault",
		      cmd_fault_set_enabled, 4, 0),
	SHELL_CMD_ARG(set_critical, &dsub_device_name, "Set fault criticality",
		      cmd_fault_set_critical, 4, 0),
#ifdef CONFIG_FAULT_MGMT_STORAGE
	SHELL_CMD_ARG(list, NULL, "List all reported faults", cmd_fault_listed, 0, 1),
	SHELL_CMD_ARG(summary, NULL, "Show fault summary", cmd_fault_summary, 0, 0),
	SHELL_CMD_ARG(count, NULL, "Total faults reported", cmd_total_reported_fault, 0, 0),
	SHELL_CMD_ARG(clear, NULL, "Clear the storage", cmd_clear_stored_fault, 0, 0),
#endif
#ifdef CONFIG_FAULT_MGMT_SAFETY
	SHELL_CMD_ARG(safety_status, &dsub_device_name, "Read safety state",
		      cmd_fault_safety_status, 2, 0),
	SHELL_CMD_ARG(safety_control, &dsub_device_name, "Send safety signal",
		      cmd_fault_safety_control, 3, 0),
#endif
	SHELL_SUBCMD_SET_END);

SHELL_CMD_REGISTER(fault, &fault, "Fault management subsystem commands", NULL);
