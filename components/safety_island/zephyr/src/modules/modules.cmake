#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: Apache-2.0

set(ZEPHYR_TRUSTED_FIRMWARE_M_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR}/trusted-firmware-m)
set(ZEPHYR_TRUSTED_FIRMWARE_M_KCONFIG ${CMAKE_CURRENT_LIST_DIR}/trusted-firmware-m/Kconfig)

set(ZEPHYR_PSA_ARCH_TESTS_CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR}/psa-arch-tests)
set(ZEPHYR_PSA_ARCH_TESTS_KCONFIG ${CMAKE_CURRENT_LIST_DIR}/psa-arch-tests/Kconfig)
