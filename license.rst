#######
License
#######

The repository’s standard license is the MIT license, under which most of the
repository’s content is provided (below).

::

    Copyright (c) <year> <copyright holders>

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to
    deal in the Software without restriction, including without limitation the
    rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
    sell copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice (including the next
    paragraph) shall be included in all copies or substantial portions of the
    Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
    IN THE SOFTWARE.


Exceptions to this standard license relate to files that represent
modifications to externally licensed works (for example, patch files). These
files may therefore be included in the repository under alternative licenses in
order to be compliant with the licensing requirements of the associated
external works.

Find below the list of licenses related to externally licensed works:

  * Apache-2.0
  * BSD-2-Clause
  * BSD-3-Clause
  * BSD-3-Clause-Clear
  * CC-BY-4.0
  * GPL-2.0
  * GPL-2.0-only
  * LGPL-2.0
  * LGPL-2.1
  * MIT

****************
SPDX Identifiers
****************

Individual files contain the following tag instead of the full license text.

::

    SPDX-License-Identifier: MIT

This enables machine processing of license information based on the SPDX
License Identifiers that are here available: http://spdx.org/licenses/
