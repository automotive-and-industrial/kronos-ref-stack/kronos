..
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

###################################################
|Arm| Kronos Reference Software Stack Documentation
###################################################

.. toctree::
   :maxdepth: 3

   overview
   user_guide/index
   design/index
   license_link
   releasenotes
