..
 # SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

############
Applications
############

.. toctree::
   :titlesonly:
   :maxdepth: 1
   :caption: Contents

   cam
   actuation
   bridge
   parsec_enabled_tls
   psa_arch_tests
