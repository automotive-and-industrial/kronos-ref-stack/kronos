..
 # SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

###############
Solution Design
###############

.. toctree::
   :titlesonly:
   :maxdepth: 1
   :caption: Contents

   boot_process
   secure_services
   secure_firmware_update
   fault_mgmt
   hipc
   components
   applications/index
   integration
   validation
   systemready_ir
