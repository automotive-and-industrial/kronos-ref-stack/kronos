..
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

##########
User Guide
##########

.. toctree::
   :maxdepth: 1
   :caption: Contents

   reproduce

Describes how to reproduce a Reference Software Stack image, and how to
configure, build, run and validate the supported set of architecture features
and :ref:`Use-Cases <overview_use_cases>`.

.. toctree::
   :maxdepth: 1

   borrow

Provides an overview of the key components and the applied changes for this
project so that the user can determine how to isolate and build them
independently and import them into their existing project.
