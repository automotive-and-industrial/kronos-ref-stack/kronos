..
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

######
Borrow
######

To reuse the components and patches of the Reference Software Stack, refer to
each of the individual components mentioned in :ref:`design_components`.

******************
Downstream Changes
******************

Detailed below is a table linking each component of the Reference Software Stack
to its Downstream Changes section. Each section contains the component's patch
files and a high level overview of their purpose/functionality as a group:

.. list-table::
  :header-rows: 1

  * - Component
    - Link to the Downstream Changes
  * - :ref:`design_components_rss`
    - :ref:`design_components_rss_downstream_changes` for RSS
  * - :ref:`design_components_scp-firmware`
    - :ref:`design_components_scp-firmware_downstream_changes` for SCP-firmware
  * - :ref:`design_components_trusted-firmware-a`
    - :ref:`design_components_trusted-firmware-a_downstream_changes` for TF-A
  * - :ref:`design_components_op-tee`
    - :ref:`design_components_op-tee_downstream_changes` for OP-TEE
  * - :ref:`design_components_trusted-services`
    - :ref:`design_components_trusted-services_downstream_changes` for Trusted
      Services
  * - :ref:`design_components_u-boot`
    - :ref:`design_components_u-boot_downstream_changes` for U-Boot
  * - :ref:`design_components_xen`
    - :ref:`design_components_xen_downstream_changes` for Xen
  * - :ref:`design_components_linux`
    - :ref:`design_components_linux_downstream_changes` for Linux
  * - :ref:`design_components_zephyr`
    - :ref:`design_components_zephyr_downstream_changes` for Zephyr
