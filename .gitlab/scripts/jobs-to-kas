#! /bin/bash
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# Read a GitLab CI job name on $1 and transform it to a
# list of Kas yaml files
set -e -u

# Read Job name from $1 and split on /
IFS=/ read -r -a PARTS<<<"${1}"

# Remove names NOT to be transformed
for i in "${!PARTS[@]}"; do
    part="${PARTS[i]}"
	if [[ $part == 'arm64' || $part == 'x86_64' ]]; then
        unset 'PARTS[i]'
	fi
done

# Prefix each part with yocto/kas/
PARTS=("${PARTS[@]/#/yocto/kas/}")

# Suffix each part with .yml
PARTS=("${PARTS[@]/%/.yml}")

# Print colon-separated with the ci only options appended
IFS=":"
echo "${PARTS[*]}:yocto/kas/ci.yml"
