#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

# /* cspell:disable-next-line */
BRANCH_CYCLONEDDS ?= "branch=master"
SRC_URI_CYCLONEDDS ?= "git://github.com/eclipse-cyclonedds/cyclonedds;${BRANCH_CYCLONEDDS};protocol=https"
SRCREV_CYCLONEDDS ?= "f7688ce709e53f408e30706ebc27bd052c03d693"
