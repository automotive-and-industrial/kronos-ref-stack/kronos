#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SRC_URI_ACTUATION ?= "git://git.gitlab.arm.com/automotive-and-industrial/safety-island/actuation-demo.git;protocol=https"
BRANCH_ACTUATION ?= "branch=main"
SRCREV_ACTUATION ?= "ef5a0affc5ecaa5a397d557cd3bc2adfeb2cc590"
