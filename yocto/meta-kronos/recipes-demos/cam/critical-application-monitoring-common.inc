#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SRC_URI_CAM ?= "git://git.gitlab.arm.com/automotive-and-industrial/safety-island/critical-app-monitoring.git;protocol=https"
BRANCH_CAM ?= "branch=main"
SRCREV_CAM ?= "e42e43837def8f406db8aed03ae5dccf568a5090"
