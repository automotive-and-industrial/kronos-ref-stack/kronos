From 6bd9dc614cd2678d118d7f9c23e8da1b7b81854b Mon Sep 17 00:00:00 2001
From: Henry Wang <Henry.Wang@arm.com>
Date: Thu, 22 Sep 2022 07:28:14 +0000
Subject: [PATCH] xen/arm, doc: Add MPAM system level cache (SLC) control

The system level cache (SLC) is a newly introduced cache in recent
Arm mesh interconnects such as CMN-700. Unlike the traditional L2
or L3 cache which are usually local to the CPU or cluster, the SLC
is located within the interconnect as HN-F (Fully-coherent home node)
protocol nodes and available to all CPUs.

Since MPAM can support the portion and patitioning of the SLC, this
commit introduces the MPAM SLC portion control support by:

- Introduce a new mpam_class type `MPAM_CLASS_SLC`.
- Introduce new resource type `slc` to MPAM Xen cmdline `dom0_mpam`
  and device tree property `xen-domain-mpam-slc-cpbm` to allow users
  to specify SLC control to dom0 and dom0less domUs.
- Extend existing MPAM internal helpers for detecting the size of
  SLC and applying SLC portion control to domains.

Signed-off-by: Henry Wang <Henry.Wang@arm.com>
Upstream-Status: Pending
Issue-Id: SCM-5135
---
 docs/misc/arm/device-tree/booting.txt |  9 +++++
 docs/misc/xen-command-line.pandoc     |  6 +++-
 xen/arch/arm/domain.c                 |  1 +
 xen/arch/arm/domain_build.c           |  4 +++
 xen/arch/arm/include/asm/mpam.h       |  3 +-
 xen/arch/arm/mpam.c                   | 50 +++++++++++++++++++++++++--
 xen/include/public/arch-arm.h         |  1 +
 7 files changed, 70 insertions(+), 4 deletions(-)

diff --git a/docs/misc/arm/device-tree/booting.txt b/docs/misc/arm/device-tree/booting.txt
index 733eed34114f..374e37ec26ae 100644
--- a/docs/misc/arm/device-tree/booting.txt
+++ b/docs/misc/arm/device-tree/booting.txt
@@ -269,6 +269,15 @@ with the following properties:
     of the system, it will be clamped to the maximum value supported
     by the system.
 
+- xen,domain-mpam-slc-cpbm
+
+    Optional. A 32-bit integer specifying the MPAM cache portion
+    bitmap (CPBM) for the domain SLC. Note that MPAM has architecture
+    maximum 32-bit CPBM, but hardware may have different implementations.
+    If the CPBM value given in the device tree exceeds the maximum value
+    of the system, it will be clamped to the maximum value supported
+    by the system.
+
 Under the "xen,domain" compatible node, one or more sub-nodes are present
 for the DomU kernel and ramdisk.
 
diff --git a/docs/misc/xen-command-line.pandoc b/docs/misc/xen-command-line.pandoc
index 5aeac07386b4..c918c6bdc0aa 100644
--- a/docs/misc/xen-command-line.pandoc
+++ b/docs/misc/xen-command-line.pandoc
@@ -2866,7 +2866,7 @@ the hypervisor was compiled with `CONFIG_XSM` enabled.
   be enabled.
 
 ### dom0_mpam (Arm64)
-> `= List of ( l3:<bitmap> )`
+> `= List of ( l3:<bitmap> | slc:<bitmap> )`
 
 Set the amount of MPAM resource limit for the initial domain (dom0). This is an
 optional command line option.
@@ -2875,3 +2875,7 @@ optional command line option.
 bitmap. System l3 cache resource is divided into numbers of portions. Each
 portion corresponds to a bit in <bitmap> and the set bit represents the
 corresponding cache portion is available.
+* `slc:<bitmap>` specifies the limit of system level cache. <bitmap> is a
+maximum 32bit bitmap. System level cache resource is divided into numbers of
+portions. Each portion corresponds to a bit in <bitmap> and the set bit
+represents the corresponding cache portion is available.
diff --git a/xen/arch/arm/domain.c b/xen/arch/arm/domain.c
index cb619121fc90..62606ceabee6 100644
--- a/xen/arch/arm/domain.c
+++ b/xen/arch/arm/domain.c
@@ -808,6 +808,7 @@ int arch_domain_create(struct domain *d,
 #ifdef CONFIG_MPAM
     d->arch.mpam.partid = d->arch.p2m.vmid;
     d->arch.mpam.cfg.l3_cpbm = config->arch.mpam.l3_cpbm;
+    d->arch.mpam.cfg.slc_cpbm = config->arch.mpam.slc_cpbm;
     if ( mpam_enabled )
         mpam_update_domain(d);
 #endif
diff --git a/xen/arch/arm/domain_build.c b/xen/arch/arm/domain_build.c
index ba20793f103d..67a6ce1d7ead 100644
--- a/xen/arch/arm/domain_build.c
+++ b/xen/arch/arm/domain_build.c
@@ -3790,6 +3790,8 @@ void __init create_domUs(void)
         {
             dt_property_read_u32(node, "xen,domain-mpam-l3-cpbm",
                                  &d_cfg.arch.mpam.l3_cpbm);
+            dt_property_read_u32(node, "xen,domain-mpam-slc-cpbm",
+                                 &d_cfg.arch.mpam.slc_cpbm);
         }
 #endif
 
@@ -3927,6 +3929,8 @@ void __init create_dom0(void)
 #ifdef CONFIG_MPAM
     dom0_cfg.arch.mpam.l3_cpbm = dom0_mpam_config.l3_cpbm ?
                                  dom0_mpam_config.l3_cpbm : 0;
+    dom0_cfg.arch.mpam.slc_cpbm = dom0_mpam_config.slc_cpbm ?
+                                  dom0_mpam_config.slc_cpbm : 0;
 #endif
 
     dom0 = domain_create(0, &dom0_cfg, CDF_privileged | CDF_directmap);
diff --git a/xen/arch/arm/include/asm/mpam.h b/xen/arch/arm/include/asm/mpam.h
index 6528d89b462d..b72179296f70 100644
--- a/xen/arch/arm/include/asm/mpam.h
+++ b/xen/arch/arm/include/asm/mpam.h
@@ -267,7 +267,8 @@
 #define MPAMIDR_PARTID_MAX     GENMASK(15, 0)
 
 enum mpam_class_types {
-    MPAM_CLASS_CACHE,       /* Caches, e.g. L2 and L3 */
+    MPAM_CLASS_CACHE,       /* CPU Caches, e.g. L2 and L3 */
+    MPAM_CLASS_SLC,         /* System Level Cache in CMN interconnects */
     MPAM_CLASS_MEMORY,      /* Main memory */
     MPAM_CLASS_UNKNOWN,     /* Everything else, e.g. SMMU */
 };
diff --git a/xen/arch/arm/mpam.c b/xen/arch/arm/mpam.c
index 8f2074fcec88..fba4b93bd7c9 100644
--- a/xen/arch/arm/mpam.c
+++ b/xen/arch/arm/mpam.c
@@ -63,6 +63,13 @@ static void __init read_cache_size_from_registers(void *arg)
     uint64_t ccsidr = 0;
     unsigned int numsets = 0, associativity = 0, linesize = 0;
 
+    if ( cache->type == MPAM_CLASS_SLC )
+    {
+        printk(XENLOG_WARNING
+               "MPAM: SLC size can only be fetched from device tree\n");
+        return;
+    }
+
     /* Select the cache_level cache in CSSELR_EL1, bits [3:1] */
     WRITE_SYSREG(regval, CSSELR_EL1);
 
@@ -98,6 +105,9 @@ static int __init parse_dom0_mpam_cmdline(const char *s)
         if ( strncmp(token, "l3", strlen("l3")) == 0 )
             /* parse_size_and_unit() default returns KB if no unit specified */
             dom0_mpam_config.l3_cpbm = size >>= 10;
+        else if ( strncmp(token, "slc", strlen("slc")) == 0 )
+            /* parse_size_and_unit() default returns KB if no unit specified */
+            dom0_mpam_config.slc_cpbm = size >>= 10;
         else
         {
             printk(XENLOG_ERR "MPAM: Invalid Dom0 MPAM cmdline\n");
@@ -383,6 +393,7 @@ mpam_ris_get_affinity(struct mpam_msc *msc, cpumask_t *affinity,
 
     switch ( type )
     {
+    case MPAM_CLASS_SLC:
     case MPAM_CLASS_CACHE:
         err = get_cpumask_from_cache_id(comp->comp_id, class->level, affinity);
         if ( err )
@@ -513,6 +524,7 @@ mpam_dt_parse_cache_resource(struct mpam_msc *msc,
     int rc = 0;
     uint32_t cache_phandle, cache_level;
     struct dt_device_node *cache_node;
+    enum mpam_class_types type;
 
     if ( dt_device_is_compatible(node, "arm,mpam-cache") )
     {
@@ -542,6 +554,9 @@ mpam_dt_parse_cache_resource(struct mpam_msc *msc,
         return -EINVAL;
     }
 
+    type = dt_device_is_compatible(cache_node, "arm,slc") ?
+           MPAM_CLASS_SLC : MPAM_CLASS_CACHE;
+
     rc = dt_property_read_u32(cache_node, "cache-level", &cache_level);
     if ( !rc )
     {
@@ -551,7 +566,7 @@ mpam_dt_parse_cache_resource(struct mpam_msc *msc,
         return -ENOENT;
     }
 
-    return mpam_ris_create(msc, ris_idx, MPAM_CLASS_CACHE, cache_level,
+    return mpam_ris_create(msc, ris_idx, type, cache_level,
                            cache_node->phandle);
 }
 
@@ -1318,6 +1333,15 @@ static void mpam_update_resources(void *data)
             mpam_cfg.cpbm = domain->cfg.l3_cpbm;
             mpam_update_class(class, domain->partid, &mpam_cfg);
         }
+
+        if ( class->type == MPAM_CLASS_SLC )
+        {
+            struct mpam_config mpam_cfg = {};
+
+            mpam_set_feature(mpam_feat_cpor_part, &mpam_cfg);
+            mpam_cfg.cpbm = domain->cfg.slc_cpbm;
+            mpam_update_class(class, domain->partid, &mpam_cfg);
+        }
     }
 }
 
@@ -1363,10 +1387,14 @@ static int __init mpam_probe_cache_info(void)
         unsigned int cpu;
         uint32_t cache_size = 0, cache_level = 0;
         struct mpam_class *cache;
+        enum mpam_class_types cache_type;
 
         if ( !dt_device_is_compatible(node, "cache") )
             continue;
 
+        cache_type = dt_device_is_compatible(node, "arm,slc") ?
+                     MPAM_CLASS_SLC : MPAM_CLASS_CACHE;
+
         rc = dt_property_read_u32(node, "cache-level", &cache_level);
         if ( !rc )
         {
@@ -1380,7 +1408,7 @@ static int __init mpam_probe_cache_info(void)
             return rc;
 
         spin_lock(&mpam_list_lock);
-        cache = mpam_class_get(cache_level, MPAM_CLASS_CACHE, false);
+        cache = mpam_class_get(cache_level, cache_type, false);
         if ( IS_ERR(cache) )
         {
             spin_unlock(&mpam_list_lock);
@@ -1434,6 +1462,9 @@ int mpam_get_hwinfo(enum mpam_class_types type, unsigned int level,
 
         switch (type)
         {
+        case MPAM_CLASS_SLC:
+            *val = class->props.cpbm_wd;
+            return 0;
         case MPAM_CLASS_CACHE:
             if ( class->level != level )
                 break;
@@ -1475,6 +1506,21 @@ void mpam_sanitise_config(struct xen_arch_domctl_mpam *cfg, bool domain_create)
         else
             cfg->l3_cpbm = domain_create ? hw_lim : cfg->l3_cpbm;
     }
+
+    if ( mpam_get_hwinfo(MPAM_CLASS_SLC, 0, &hw_val) )
+    {
+        if ( cfg->slc_cpbm )
+            printk(XENLOG_ERR "MPAM: SLC CPBM not supported\n");
+    }
+    else
+    {
+        hw_lim = (1 << hw_val) - 1;
+
+        if ( cfg->slc_cpbm )
+            cfg->slc_cpbm = (cfg->slc_cpbm > hw_lim) ? hw_lim : cfg->slc_cpbm;
+        else
+            cfg->slc_cpbm = domain_create ? hw_lim : cfg->slc_cpbm;
+    }
 }
 
 void mpam_update_domain(struct domain *d)
diff --git a/xen/include/public/arch-arm.h b/xen/include/public/arch-arm.h
index 6f1a4bb642ef..5d9f2b7a0b25 100644
--- a/xen/include/public/arch-arm.h
+++ b/xen/include/public/arch-arm.h
@@ -329,6 +329,7 @@ DEFINE_XEN_GUEST_HANDLE(vcpu_guest_context_t);
 
 struct xen_arch_domctl_mpam {
     uint32_t l3_cpbm;
+    uint32_t slc_cpbm;
 };
 
 struct xen_arch_domainconfig {
-- 
2.34.1

