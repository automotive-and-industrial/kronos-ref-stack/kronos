#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

class KronosConfig:
    si_cl1_ipaddr = '192.168.1.1'
    si_cl1_console_name = 'safety_island_c1'
    domu1_hostname = r'domu1'
    domu2_hostname = r'domu2'
    dom0_prompt = r'root@fvp-rd-kronos:~#'
    baremetal_prompt = r'root@fvp-rd-kronos:~#'
