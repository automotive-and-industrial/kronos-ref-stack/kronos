#
# SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

from oeqa.runtime.cases.fvp_devices import FvpDevicesTest


class KronosFvpDevicesTest(FvpDevicesTest):
    pass
