#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

LIC_FILES_CHKSUM = "file://COPYING.MPL2;md5=815ca599c9df247a0c7f619bab123dad \
                    file://COPYING.BSD;md5=543367b8e11f07d353ef894f71b574a0 \
                    file://COPYING.GPL;md5=d32239bcb673463ab874e80d47fae504 \
                    file://COPYING.LGPL;md5=4fbd65380cdd255951079008b364516c \
                    file://COPYING.MINPACK;md5=5fe4603e80ef7390306f51ef74449bbd \
"

SRC_URI = "git://gitlab.com/libeigen/eigen.git;protocol=http;branch=3.3"

SRCREV = "21ae2afd4edaa1b69782c67a54182d34efe43f9c"

PV = "3.3.7"
